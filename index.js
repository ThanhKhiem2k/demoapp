import 'react-native-gesture-handler';
import {AppRegistry} from 'react-native';
import {LogBox} from 'react-native';
import App from './src/app';
import {name as appName} from './app.json';
LogBox.ignoreLogs(['new NativeEventEmitter']); // Ignore log notification by message
LogBox.ignoreAllLogs();
LogBox.ignoreLogs(['Warning: ...']);

AppRegistry.registerComponent(appName, () => App);
