import React, {useEffect, useState} from 'react';
import {
  View,
  SafeAreaView,
  TouchableOpacity,
  Text,
  Image,
  ImageBackground,
  StyleSheet,
  // Platform,
  useWindowDimensions,
  Keyboard,
  Platform,
  KeyboardAvoidingView,
  // Alert,
  // ActionSheetIOS,
} from 'react-native';
import {EventType, useZoom} from '@zoom/react-native-videosdk';
import {Icon} from '../../components/icon';
import Carousel from 'react-native-reanimated-carousel';
// import {
//   checkMultiple,
//   requestMultiple,
//   openSettings,
//   PERMISSIONS,
//   RESULTS,
//   Permission,
//   PermissionStatus,
// } from 'react-native-permissions';
import Animated, {
  Extrapolate,
  interpolate,
  useAnimatedStyle,
  useSharedValue,
} from 'react-native-reanimated';
import CheckStatusPermission from '../../components/check-status-permission/checkStatusPermission';
import {TextInput} from 'react-native-paper';
import AsyncStorage from '@react-native-async-storage/async-storage';

const introImages = [
  /* TODO: Hide for now since we do not support other rendering methods.
  Platform.OS === 'ios'
    ? require('./images/intro-image1.png')
    : require('./images/intro-image1-android.png'),
   */
  require('./images/intro-image2.png'),
  require('./images/intro-image3.png'),
  require('./images/intro-image4.png'),
  require('./images/intro-image5.png'),
  require('./images/intro-image6.png'),
  require('./images/intro-image7.png'),
];

interface Props {
  navigation: any;
}

export function IntroScreen({navigation}: Props) {
  const carouselWidth = useWindowDimensions().width;
  const introIndex = useSharedValue<number>(0);
  const [text, setText] = useState<string>('');
  const [extensionData, setExtensionData] = useState<string>('');
  useEffect(() => {
    const unsubscribe = navigation.addListener('focus', () => {
      getNameCurent();
    });
    return () => {
      unsubscribe();
    };
  }, []);

  return (
    <ImageBackground
      style={styles.container}
      source={require('./images/intro-bg.png')}>
      {/* <CheckStatusPermission
        openPopupSettingApp={false}
        callbackFunction={undefined}
      /> */}
      <SafeAreaView style={styles.container} />
      <View style={styles.topWrapper}>
        {/* <Icon
            style={styles.hambergerMenu}
            name="hamburger"
            onPress={navigation.openDrawer}
          /> */}
        {!!introIndex && (
          <View style={styles.viewListNumber}>
            {introImages.map((_, index) => {
              return (
                <PaginationItem
                  activeDot={introIndex}
                  width={carouselWidth / 40}
                  index={index}
                  key={index}
                  length={introImages.length}
                />
              );
            })}
          </View>
        )}
      </View>
      <Carousel
        loop
        width={carouselWidth}
        height={carouselWidth * 2}
        pagingEnabled={true}
        snapEnabled={true}
        autoPlay={true}
        data={introImages}
        mode="parallax"
        modeConfig={{
          parallaxScrollingScale: 0.9,
          parallaxScrollingOffset: 50,
        }}
        scrollAnimationDuration={3000}
        onProgressChange={(_, index) => {
          if (Number.isInteger(index)) {
            introIndex.value = index;
          }
        }}
        renderItem={({item}) => (
          <Image style={styles.introImage} source={item} />
        )}
      />
      <View style={styles.bottomWrapper} pointerEvents="box-none">
        <Image source={require('./images/curve-mask.png')} />
        <KeyboardAvoidingView
          behavior="padding"
          keyboardVerticalOffset={Platform.OS === 'ios' ? 0 : -100}
          style={{backgroundColor: 'white'}}>
          <View style={styles.buttonWrapper}>
            <Text
              style={{
                fontSize: 30,
                marginBottom: 20,
                width: '100%',
                textAlign: 'center',
                color: '#0a5081ff',
              }}>
              {extensionData === '' ? 'Register' : 'Login'}
            </Text>
            <TextInput
              mode="outlined"
              label="My Name"
              value={text}
              placeholder="Enter your name..."
              style={styles.textInputCreatName}
              onChangeText={_text => setText(_text)}
              autoFocus={true}
            />
            <TouchableOpacity
              style={styles.createButton}
              onPress={() => {
                AsyncStorage.setItem('ExtensionData', text);
                navigation.navigate('Join', {name: text});
              }}>
              <Text style={styles.createText}>
                {extensionData === ''
                  ? 'Create'
                  : text !== extensionData
                  ? 'Change Name'
                  : 'Next'}
              </Text>
            </TouchableOpacity>
          </View>
        </KeyboardAvoidingView>
      </View>
      {/* </SafeAreaView> */}
    </ImageBackground>
  );

  function getNameCurent() {
    AsyncStorage.getItem('ExtensionData').then(valueCurrent => {
      if (valueCurrent !== null) {
        setExtensionData(valueCurrent);
        setText(valueCurrent);
      }
    });
  }
}

const PaginationItem: React.FC<{
  index: number;
  length: number;
  width: number;
  activeDot: Animated.SharedValue<number>;
}> = props => {
  const {activeDot, index, length, width} = props;

  const animStyle = useAnimatedStyle(() => {
    let inputRange = [index - 1, index, index + 1];
    let outputRange = [-width, 0, width];

    if (index === 0 && activeDot?.value > length - 1) {
      inputRange = [length - 1, length, length + 1];
      outputRange = [-width, 0, width];
    }

    return {
      transform: [
        {
          translateX: interpolate(
            activeDot?.value,
            inputRange,
            outputRange,
            Extrapolate.CLAMP,
          ),
        },
      ],
    };
  }, [activeDot, index, length]);
  return (
    <View
      style={{
        backgroundColor: 'white',
        width,
        height: width,
        borderRadius: 50,
        overflow: 'hidden',
      }}>
      <Animated.View
        style={[
          {
            borderRadius: 50,
            backgroundColor: '#26292E',
            flex: 1,
          },
          animStyle,
        ]}
      />
    </View>
  );
};

const styles = StyleSheet.create({
  textInputCreatName: {
    minHeight: 50,
    paddingHorizontal: 20,
    borderRadius: 12,
    marginHorizontal: 30,
    marginBottom: 10,
  },
  viewListNumber: {
    marginTop: 20,
    flexDirection: 'row',
    justifyContent: 'space-between',
    width: 100,
    alignSelf: 'center',
  },
  container: {
    flex: 1,
  },
  topWrapper: {
    flexDirection: 'row',
    justifyContent: 'center',
  },
  hambergerMenu: {
    width: 24,
    height: 24,
    marginLeft: 16,
    marginTop: 16,
  },
  padding: {
    width: 24,
    marginLeft: 16,
  },
  introImage: {
    width: '100%',
    height: '70%',
    marginTop: -40,
    alignSelf: 'center',
    resizeMode: 'contain',
  },
  dotStyle: {
    width: 7,
    height: 7,
    borderRadius: 5,
    marginHorizontal: 0,
    backgroundColor: '#FFF',
  },
  bottomWrapper: {
    position: 'absolute',
    width: '100%',
    height: '100%',
    justifyContent: 'flex-end',
  },
  buttonWrapper: {
    paddingTop: 14,
    backgroundColor: '#FFF',
  },
  createButton: {
    marginHorizontal: 30,
    paddingVertical: 12,
    alignItems: 'center',
    borderRadius: 12,
    backgroundColor: '#0070F3',
    marginTop: 8,
    marginBottom: 32,
  },
  createText: {
    fontSize: 18,
    color: '#FFF',
  },
  joinButton: {
    marginTop: 8,
    marginBottom: 32,
    paddingVertical: 8,
    alignItems: 'center',
  },
  joinText: {
    paddingVertical: 12,
    fontSize: 18,
    color: '#0070F3',
  },
  content: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  input: {
    height: 40,
    borderColor: 'gray',
    borderWidth: 1,
    width: '80%',
    paddingHorizontal: 10,
  },
});
