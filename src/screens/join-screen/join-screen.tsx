import React, {useEffect, useState} from 'react';
import {View, TouchableOpacity, StyleSheet, Text, Alert} from 'react-native';
import {TextInputRow} from '../../components/text-input-row';
import CheckStatusPermission from '../../components/check-status-permission/checkStatusPermission';
import AsyncStorage from '@react-native-async-storage/async-storage';

const DEFAULT_SESSION_NAMES = [
  'grand-canyon',
  'yosemite',
  'yellowstone',
  'disneyland',
  'golden-gate-bridge',
  'monument-valley',
  'death-valley',
  'brooklyn-bridge',
  'hoover-dam',
  'lake-tahoe',
];

type JoinScreenProps = {
  route: any;
  navigation: any;
};

export function JoinScreen({route, navigation}: JoinScreenProps) {
  const [sessionName, setSessionName] = useState('');
  const [sessionPassword, setSessionPassword] = useState('');
  const [displayName, setDisplayName] = useState('');
  // const [sessionIdleTimeoutMins, setSessionIdleTimeoutMins] = useState('40');
  // const [roleType, setRoleType] = useState('1');
  const sessionIdleTimeoutMins = '40';
  const roleType = '1';

  const NameID = route?.params?.name;
  useEffect(() => {
    navigation.setOptions({
      title: 'Call Option',
    });
    setDisplayName(NameID);
    const defaultSessionName = Math.floor(100000 + Math.random() * 900000);
    setSessionName(defaultSessionName.toString().trim());
  }, []);

  const checkTextInput = () => {
    if (!sessionName.trim()) {
      Alert.alert('Please Enter Session Name');
      return;
    }
    if (!displayName.trim()) {
      Alert.alert('Please Enter Display Name');
      return;
    }
    if (!roleType.trim()) {
      Alert.alert('Please Enter Role Type');
      return;
    }
    navigation.navigate('Call', {
      sessionName,
      displayName,
      sessionPassword,
      roleType,
      sessionIdleTimeoutMins,
    });
  };
  // const [funcHere, setFunction] = useState<Function>();
  // const callbackFunction = (childFunction: Function) => {
  //   setFunction(childFunction);
  // };
  return (
    <View style={styles.container}>
      <CheckStatusPermission
        openPopupSettingApp={true}
        callbackFunction={undefined}
      />
      <TextInputRow
        label="ID"
        placeholder="Required"
        keyboardType="default"
        value={sessionName}
        onChangeText={setSessionName}
        allowEditText={false}
      />
      <TextInputRow
        label="Display Name"
        placeholder="Required"
        keyboardType="default"
        value={NameID}
        onChangeText={setDisplayName}
        allowEditText={false}
      />
      <TextInputRow
        label="Password"
        placeholder="Optional"
        keyboardType="default"
        value={sessionPassword}
        onChangeText={setSessionPassword}
        secureTextEntry
      />
      <TouchableOpacity onPress={checkTextInput} style={styles.button}>
        <Text style={styles.buttonText}>Create Call</Text>
      </TouchableOpacity>
    </View>
  );
}

const styles = StyleSheet.create({
  ball: {
    height: 150,
    width: 150,
  },
  container: {
    flex: 1,
    backgroundColor: 'white',
  },
  button: {
    backgroundColor: '#0e71eb',
    alignItems: 'center',
    marginTop: 15,
    marginHorizontal: 15,
    paddingVertical: 10,
    borderRadius: 5,
  },
  buttonText: {
    color: 'white',
  },
});
