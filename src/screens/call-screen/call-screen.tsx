import React, {useEffect, useState, useRef} from 'react';
import {
  Alert,
  SafeAreaView,
  View,
  Text,
  TouchableOpacity,
  StyleSheet,
  Image,
  Platform,
  BackHandler,
  AppState,
} from 'react-native';
import {VideoView} from '../../components/video-view';
import {icons} from '../../components/icon';
import {useIsMounted} from '../../utils/hooks';
import generateJwt from '../../utils/jwt';
import {useSharedValue, Easing, withTiming} from 'react-native-reanimated';
import {CirclesLoader} from 'react-native-indicator';
import {
  EventType,
  useZoom,
  ZoomVideoSdkUser,
  ZoomVideoSdkUserType,
  // ZoomVideoSdkChatMessage,
  // ZoomVideoSdkChatMessageType,
  // ZoomVideoSdkLiveTranscriptionMessageInfo,
  // ZoomVideoSdkLiveTranscriptionMessageInfoType,
  // ChatMessageDeleteType,
  // ShareStatus,
  // LiveStreamStatus,
  Errors,
  // PhoneFailedReason,
  // PhoneStatus,
  // VideoPreferenceMode,
  // LiveTranscriptionStatus,
  MultiCameraStreamStatus,
  // SystemPermissionType,
  // NetworkStatus,
} from '@zoom/react-native-videosdk';
import NetInfo from '@react-native-community/netinfo';
import {RNKeyboard, SoftInputMode} from 'react-native-keyboard-area';
import CheckStatusPermission from '../../components/check-status-permission/checkStatusPermission';
type CallScreenProps = {
  navigation: any;
  route: any;
};

export function CallScreen({navigation, route}: CallScreenProps) {
  const [isInSession, setIsInSession] = useState(false);
  const [users, setUsersInSession] = useState<ZoomVideoSdkUser[]>([]);
  const [fullScreenUser, setFullScreenUser] = useState<ZoomVideoSdkUser>();
  const [statusVideoCall, setStatusVideoCall] = useState<boolean>(true);
  const [funcHere, setFunction] = useState<Function>();
  // const [chatMessages, setChatMessages] = useState<ZoomVideoSdkChatMessage[]>(
  //   [],
  // );
  const [isHidden, setIsHidden] = useState(false);
  const [isMuted, setIsMuted] = useState(true);
  const [isVideoOn, setIsVideoOn] = useState(false);
  // const isLongTouchRef = useRef(false);
  const videoInfoTimer = useRef<any>(0);
  // react-native-reanimated issue: https://github.com/software-mansion/react-native-reanimated/issues/920
  // Not able to reuse animated style in multiple views.
  const uiOpacity = useSharedValue(0);
  const inputOpacity = useSharedValue(0);
  const isMounted = useIsMounted();
  const zoom = useZoom();
  const [isOriginalAspectRatio, setIsOriginalAspectRatio] =
    useState<boolean>(false);
  const [internetConnectStatus, setInternetConnectStatus] =
    useState<boolean>(true);

  useEffect(() => {
    (async () => {
      const {params} = route;
      const token = await generateJwt(params.sessionName, params.roleType);
      try {
        await zoom.joinSession({
          sessionName: params.sessionName,
          sessionPassword: params.sessionPassword,
          token: token,
          userName: params.displayName,
          audioOptions: {
            connect: true,
            mute: true,
          },
          videoOptions: {
            localVideoOn: true,
          },
          sessionIdleTimeoutMins: parseInt(params.sessionIdleTimeoutMins, 10),
        });
      } catch (e) {
        console.log(e);
        setLeaveCall(true);
        Alert.alert('Failed to join the session');
      }
    })();

    if (Platform.OS === 'android') {
      RNKeyboard.setWindowSoftInputMode(
        SoftInputMode.SOFT_INPUT_ADJUST_NOTHING,
      );
    }

    return () => {
      if (Platform.OS === 'android') {
        RNKeyboard.setWindowSoftInputMode(
          SoftInputMode.SOFT_INPUT_ADJUST_RESIZE,
        );
      }
    };
  }, []);
  let waitingConnectFps = 0;
  useEffect(() => {
    const updateVideoInfo = () => {
      videoInfoTimer.current = setTimeout(async () => {
        if (!isMounted()) {
          return;
        }
        const videoOn = await fullScreenUser?.videoStatus.isOn();
        // Video statistic info doesn't update when there's no remote users
        if (!fullScreenUser || !videoOn || users.length < 2) {
          clearTimeout(videoInfoTimer.current);
          setStatusVideoCall(true);
          return;
        }
        const fps = await fullScreenUser.videoStatisticInfo.getFps();
        // console.log('fps', fps);
        waitingConnectFps++;
        if (fps === 0 && waitingConnectFps >= 3) {
          setStatusVideoCall(false);
        } else {
          setStatusVideoCall(true);
        }
        updateVideoInfo();
      }, 1000);
    };

    updateVideoInfo();

    return () => clearTimeout(videoInfoTimer.current);
  }, [fullScreenUser, users, isMounted]);

  useEffect(() => {
    if (users.length === 2) {
      setTimeout(() => {
        setFullScreenUser(users[1]);
      }, 2000);
    }
  }, [users]);

  const [leaveCall, setLeaveCall] = useState<boolean>(false);
  useEffect(() => {
    if (leaveCall === true) {
      leaveSession(true);
    }
  }, [leaveCall]);

  useEffect(() => {
    if (internetConnectStatus === false) {
      Alert.alert('No internet connection!');
    }
  }, [internetConnectStatus]);

  useEffect(() => {
    NetInfo.addEventListener(state => {
      if (state.isConnected === false) {
        setInternetConnectStatus(false);
      } else {
        setInternetConnectStatus(true);
      }
    });
    const handleBackPress = () => {
      setLeaveCall(true);
      return true;
    };

    BackHandler.addEventListener('hardwareBackPress', handleBackPress);
    const subscription = AppState.addEventListener('change', appState => {
      if (appState !== 'active') {
        return;
      }
      subscription.remove();
    });
    return () => {
      BackHandler.removeEventListener('hardwareBackPress', handleBackPress);
    };
  }, []);

  useEffect(() => {
    const sessionJoinListener = zoom.addListener(
      EventType.onSessionJoin,
      async (session: any) => {
        setIsInSession(true);
        toggleUI();
        zoom.session.getSessionName();
        const mySelf: ZoomVideoSdkUser = new ZoomVideoSdkUser(session.mySelf);
        const remoteUsers: ZoomVideoSdkUser[] =
          await zoom.session.getRemoteUsers();
        const muted = await mySelf.audioStatus.isMuted();
        const videoOn = await mySelf.videoStatus.isOn();
        const speakerOn = await zoom.audioHelper.getSpeakerStatus();
        const originalAspectRatio =
          await zoom.videoHelper.isOriginalAspectRatioEnabled();
        setUsersInSession([mySelf, ...remoteUsers]);
        setIsMuted(muted);
        setIsVideoOn(videoOn);
        setIsOriginalAspectRatio(originalAspectRatio);
      },
    );

    const sessionLeaveListener = zoom.addListener(
      EventType.onSessionLeave,
      async () => {
        setIsInSession(false);
        setUsersInSession([]);
        setLeaveCall(true);
      },
    );

    const sessionNeedPasswordListener = zoom.addListener(
      EventType.onSessionNeedPassword,
      () => {
        Alert.alert('SessionNeedPassword');
      },
    );

    const sessionPasswordWrongListener = zoom.addListener(
      EventType.onSessionPasswordWrong,
      () => {
        Alert.alert('SessionPasswordWrong');
      },
    );

    const userVideoStatusChangedListener = zoom.addListener(
      EventType.onUserVideoStatusChanged,
      async ({changedUsers}: {changedUsers: ZoomVideoSdkUserType[]}) => {
        const mySelf: ZoomVideoSdkUser = new ZoomVideoSdkUser(
          await zoom.session.getMySelf(),
        );
        changedUsers.map((u: ZoomVideoSdkUserType) => {
          if (mySelf.userId === u.userId) {
            mySelf.videoStatus.isOn().then((on: boolean) => setIsVideoOn(on));
          }
        });
        // setFullScreenUser(mySelf);
      },
    );

    const userAudioStatusChangedListener = zoom.addListener(
      EventType.onUserAudioStatusChanged,
      async ({changedUsers}: {changedUsers: ZoomVideoSdkUserType[]}) => {
        const mySelf: ZoomVideoSdkUser = new ZoomVideoSdkUser(
          await zoom.session.getMySelf(),
        );
        changedUsers.map((u: ZoomVideoSdkUserType) => {
          if (mySelf.userId === u.userId) {
            mySelf.audioStatus
              .isMuted()
              .then((muted: boolean) => setIsMuted(muted));
          }
        });
      },
    );

    const userJoinListener = zoom.addListener(
      EventType.onUserJoin,
      async ({remoteUsers}: {remoteUsers: ZoomVideoSdkUserType[]}) => {
        if (!isMounted()) {
          return;
        }
        const mySelf: ZoomVideoSdkUser = await zoom.session.getMySelf();
        const remote: ZoomVideoSdkUser[] = remoteUsers.map(
          (user: ZoomVideoSdkUserType) => new ZoomVideoSdkUser(user),
        );
        setUsersInSession([mySelf, ...remote]);
        // setFullScreenUser(mySelf);
      },
    );

    const userLeaveListener = zoom.addListener(
      EventType.onUserLeave,
      async ({
        remoteUsers,
        leftUsers,
      }: {
        remoteUsers: ZoomVideoSdkUserType[];
        leftUsers: ZoomVideoSdkUserType[];
      }) => {
        if (!isMounted()) {
          return;
        }
        setLeaveCall(true);
        const mySelf: ZoomVideoSdkUser = await zoom.session.getMySelf();
        const remote: ZoomVideoSdkUser[] = remoteUsers.map(
          (user: ZoomVideoSdkUserType) => new ZoomVideoSdkUser(user),
        );
        if (fullScreenUser) {
          leftUsers.map((user: ZoomVideoSdkUserType) => {
            if (fullScreenUser.userId === user.userId) {
              // setFullScreenUser(mySelf);
              return;
            }
          });
        } else {
          // setFullScreenUser(mySelf);
        }
        setUsersInSession([mySelf, ...remote]);
      },
    );

    const userNameChangedListener = zoom.addListener(
      EventType.onUserNameChanged,
      async ({changedUser}) => {
        setUsersInSession(
          users.map((u: ZoomVideoSdkUser) => {
            if (u && u.userId === changedUser.userId) {
              return new ZoomVideoSdkUser(changedUser);
            }
            return u;
          }),
        );
      },
    );

    // const commandReceived = zoom.addListener(
    //   EventType.onCommandReceived,
    //   (params: {sender: string; command: string}) => {
    //     console.log(
    //       'sender: ' + params.sender + ', command: ' + params.command,
    //     );
    //   },
    // );

    // const chatNewMessageNotify = zoom.addListener(
    //   EventType.onChatNewMessageNotify,
    //   (newMessage: ZoomVideoSdkChatMessageType) => {
    //     if (!isMounted()) {
    //       return;
    //     }
    //     setChatMessages([
    //       new ZoomVideoSdkChatMessage(newMessage),
    //       ...chatMessages,
    //     ]);
    //   },
    // );

    // const chatDeleteMessageNotify = zoom.addListener(
    //   EventType.onChatDeleteMessageNotify,
    //   (params: {messageID: string; deleteBy: ChatMessageDeleteType}) => {
    //     console.log(
    //       'onChatDeleteMessageNotify: messageID: ' +
    //         params.messageID +
    //         ', deleteBy: ' +
    //         params.deleteBy,
    //     );
    //   },
    // );

    // const liveStreamStatusChangeListener = zoom.addListener(
    //   EventType.onLiveStreamStatusChanged,
    //   ({status}: {status: LiveStreamStatus}) => {
    //     console.log(`onLiveStreamStatusChanged: ${status}`);
    //   },
    // );

    // const liveTranscriptionStatusChangeListener = zoom.addListener(
    //   EventType.onLiveTranscriptionStatus,
    //   ({status}: {status: LiveTranscriptionStatus}) => {
    //     console.log(`onLiveTranscriptionStatus: ${status}`);
    //   },
    // );

    // const liveTranscriptionMsgInfoReceivedListener = zoom.addListener(
    //   EventType.onLiveTranscriptionMsgInfoReceived,
    //   ({
    //     messageInfo,
    //   }: {
    //     messageInfo: ZoomVideoSdkLiveTranscriptionMessageInfoType;
    //   }) => {
    //     console.log(messageInfo);
    //     const message = new ZoomVideoSdkLiveTranscriptionMessageInfo(
    //       messageInfo,
    //     );
    //     console.log(
    //       `onLiveTranscriptionMsgInfoReceived: ${message.messageContent}`,
    //     );
    //   },
    // );

    // const networkStatusChangeListener = zoom.addListener(
    //   EventType.onUserVideoNetworkStatusChanged,
    //   async ({
    //     user,
    //     status,
    //   }: {
    //     user: ZoomVideoSdkUser;
    //     status: NetworkStatus;
    //   }) => {
    //     const networkUser: ZoomVideoSdkUser = new ZoomVideoSdkUser(user);
    //     if (status === NetworkStatus.Bad) {
    //       console.log(
    //         `onUserVideoNetworkStatusChanged: status= ${status}, user= ${networkUser.userName}`,
    //       );
    //     }
    //   },
    // );

    // const inviteByPhoneStatusListener = zoom.addListener(
    //   EventType.onInviteByPhoneStatus,
    //   (params: {status: PhoneStatus; reason: PhoneFailedReason}) => {
    //     console.log(params);
    //     console.log('status: ' + params.status + ', reason: ' + params.reason);
    //   },
    // );

    const multiCameraStreamStatusChangedListener = zoom.addListener(
      EventType.onMultiCameraStreamStatusChanged,
      ({
        status,
        changedUser,
      }: {
        status: MultiCameraStreamStatus;
        changedUser: ZoomVideoSdkUser;
      }) => {
        users.map((u: ZoomVideoSdkUserType) => {
          if (changedUser.userId === u.userId) {
            if (status === MultiCameraStreamStatus.Joined) {
              u.hasMultiCamera = true;
            } else if (status === MultiCameraStreamStatus.Left) {
              u.hasMultiCamera = false;
            }
          }
        });
      },
    );

    // const requireSystemPermission = zoom.addListener(
    //   EventType.onRequireSystemPermission,
    //   ({permissionType}: {permissionType: SystemPermissionType}) => {
    //     switch (permissionType) {
    //       case SystemPermissionType.Camera:
    //         Alert.alert(
    //           "Can't Access Camera",
    //           'please turn on the toggle in system settings to grant permission',
    //         );
    //         break;
    //       case SystemPermissionType.Microphone:
    //         Alert.alert(
    //           "Can't Access Camera",
    //           'please turn on the toggle in system settings to grant permission',
    //         );
    //         break;
    //     }
    //   },
    // );

    const eventErrorListener = zoom.addListener(
      EventType.onError,
      async (error: any) => {
        console.log('Error: ' + JSON.stringify(error));
        switch (error.errorType) {
          case Errors.SessionJoinFailed:
            setLeaveCall(true);
            Alert.alert('Failed to join the session');
            break;
          default:
        }
      },
    );

    return () => {
      sessionJoinListener.remove();
      sessionLeaveListener.remove();
      sessionPasswordWrongListener.remove();
      sessionNeedPasswordListener.remove();
      userVideoStatusChangedListener.remove();
      userAudioStatusChangedListener.remove();
      userJoinListener.remove();
      userLeaveListener.remove();
      userNameChangedListener.remove();
      multiCameraStreamStatusChangedListener.remove();
      eventErrorListener.remove();
      //chatNewMessageNotify.remove();
      // liveStreamStatusChangeListener.remove();
      // inviteByPhoneStatusListener.remove();
      // commandReceived.remove();
      // chatDeleteMessageNotify.remove();
      // liveTranscriptionStatusChangeListener.remove();
      // liveTranscriptionMsgInfoReceivedListener.remove();
      // requireSystemPermission.remove();
      // networkStatusChangeListener.remove();
    };
  }, [zoom, route, users, isMounted]);

  const toggleUI = () => {
    setTimeout(() => {
      if (isHidden === true) {
        setIsHidden(false);
      } else {
        setIsHidden(true);
      }
    }, 150);
    const easeIn = Easing.in(Easing.exp);
    const easeOut = Easing.out(Easing.exp);
    uiOpacity.value = withTiming(uiOpacity.value === 0 ? 100 : 0, {
      duration: 300,
      easing: uiOpacity.value === 0 ? easeIn : easeOut,
    });
    inputOpacity.value = withTiming(inputOpacity.value === 0 ? 100 : 0, {
      duration: 300,
      easing: inputOpacity.value === 0 ? easeIn : easeOut,
    });
  };

  const leaveSession = (endSession: boolean) => {
    zoom.leaveSession(endSession);
    console.log('12397163912');
    navigation.goBack();
  };

  const onPressAudio = async () => {
    if (funcHere) {
      funcHere();
    }
    const mySelf = await zoom.session.getMySelf();
    const muted = await mySelf.audioStatus.isMuted();
    setIsMuted(muted);
    muted
      ? await zoom.audioHelper.unmuteAudio(mySelf.userId)
      : await zoom.audioHelper.muteAudio(mySelf.userId);
  };

  const onPressVideo = async () => {
    if (funcHere) {
      funcHere();
    }
    const mySelf = await zoom.session.getMySelf();
    const videoOn = await mySelf.videoStatus.isOn();
    setIsVideoOn(videoOn);
    videoOn
      ? await zoom.videoHelper.stopVideo()
      : await zoom.videoHelper.startVideo();
  };

  const onPressLeave = async () => {
    setLeaveCall(true);
  };

  const contentStyles = {
    ...styles.container,
  };
  const microphoneStyle = {
    height: '40%',
    width: '40%',
    tintColor: isMuted ? 'red' : 'white',
  };
  const callbackFunction = (childFunction: Function) => {
    setFunction(childFunction);
  };

  return (
    <View style={styles.backgroundApp}>
      <CheckStatusPermission
        callbackFunction={callbackFunction}
        openPopupSettingApp={true}
      />
      <SafeAreaView style={styles.safeArea}>
        <View style={contentStyles}>
          {users[1] && fullScreenUser ? (
            <View style={styles.fullScreenVideo}>
              <VideoView
                preview={false}
                hasMultiCamera={false}
                multiCameraIndex={'0'}
                user={fullScreenUser}
                onPress={() => {}}
                fullScreen
              />
            </View>
          ) : null}
          {isInSession && users[0] && (
            <View style={styles.miniScreenVideo}>
              <VideoView user={users[0]} onPress={() => {}} />
            </View>
          )}
          <SafeAreaView style={styles.safeArea} pointerEvents="box-none">
            {!isInSession ||
            (statusVideoCall === false && fullScreenUser) ||
            !internetConnectStatus ? (
              <View style={styles.connectingWrapper}>
                <CirclesLoader size={50} />
                <Text style={styles.connectingText}>Connecting...</Text>
              </View>
            ) : !fullScreenUser ? (
              <View style={styles.connectingWrapper}>
                <CirclesLoader size={50} />
                <Text style={styles.connectingText}>Calling...</Text>
              </View>
            ) : null}
          </SafeAreaView>
        </View>
        <View style={styles.ListButton}>
          <TouchableOpacity
            activeOpacity={1}
            onPress={onPressAudio}
            style={styles.IconButton}>
            <Image
              style={[styles.ImageButton, microphoneStyle]}
              source={icons[isMuted ? 'microphoneOff' : 'microphoneOn']}
            />
          </TouchableOpacity>
          <TouchableOpacity
            activeOpacity={1}
            onPress={onPressVideo}
            style={styles.IconButton}>
            <Image
              style={[
                styles.ImageButton,
                {tintColor: isVideoOn ? '#fff' : 'red'},
              ]}
              source={icons[isVideoOn ? 'videoCamera' : 'videoCameraOff']}
            />
          </TouchableOpacity>
          <TouchableOpacity
            onPress={() => {
              if (isVideoOn === true) {
                zoom.videoHelper.switchCamera();
              }
            }}
            style={styles.IconButton}>
            <Image style={styles.ImageButton} source={icons.switchCameraNew} />
          </TouchableOpacity>
          <TouchableOpacity
            onPress={onPressLeave}
            style={[styles.IconButton, {backgroundColor: 'red'}]}>
            <Image
              style={styles.ImageButton}
              source={icons[isMuted ? 'hangUp' : 'hangUp']}
            />
          </TouchableOpacity>
        </View>
      </SafeAreaView>
    </View>
  );
}

const styles = StyleSheet.create({
  backgroundApp: {
    width: '100%',
    height: '100%',
    backgroundColor: '#fbd6b1',
    alignItems: 'center',
  },
  container: {
    flex: 1,
    width: '95%',
    backgroundColor: '#7b7b7b',
    borderRadius: 30,
    marginTop: 10,
    justifyContent: 'center',
    alignItems: 'center',
  },
  fullScreenVideo: {
    width: '100%',
    height: '100%',
    borderRadius: 30,
    overflow: 'hidden',
    borderWidth: 2,
    borderColor: 'white',
    // position: 'absolute',
    // zIndex: 1,
  },
  miniScreenVideo: {
    overflow: 'hidden',
    position: 'absolute',
    width: 120,
    height: 120,
    margin: 20,
    right: 0,
    top: 0,
  },
  connectingWrapper: {
    justifyContent: 'center',
    alignItems: 'center',
  },
  connectingText: {
    fontSize: 24,
    fontWeight: 'bold',
    color: '#FFF',
    marginTop: 20,
  },
  safeArea: {
    width: '100%',
    height: '100%',
    justifyContent: 'center',
    alignItems: 'center',
    position: 'absolute',
  },
  contents: {
    flex: 1,
    alignItems: 'stretch',
  },
  sessionInfo: {
    width: 200,
    padding: 8,
    borderRadius: 8,
    backgroundColor: '#4f4f4f',
  },
  sessionName: {
    fontSize: 16,
    fontWeight: 'bold',
    color: '#FFF',
  },
  videoInfo: {
    paddingHorizontal: 16,
    paddingVertical: 8,
    borderRadius: 8,
    alignItems: 'center',
    backgroundColor: 'rgba(0,0,0,0.6)',
  },
  ListButton: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    width: '100%',
    margin: 15,
  },
  IconButton: {
    height: 65,
    width: 65,
    borderRadius: 40,
    backgroundColor: '#e7802f',
    margin: 5,
    justifyContent: 'center',
    alignItems: 'center',
  },
  ImageButton: {
    tintColor: '#fff',
    height: '50%',
    width: '50%',
  },
});
